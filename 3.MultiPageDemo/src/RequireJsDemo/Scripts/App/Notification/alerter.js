﻿define(function () {

    var showGreeting = function () {
        alert('Hello!');
    };

    var showMessage = function(message) {
        alert(message);
    };

    return {
        ShowGreeting: showGreeting,
        ShowMessage: showMessage
    };
    
});