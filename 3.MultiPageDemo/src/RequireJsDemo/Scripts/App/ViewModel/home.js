﻿define(['Notification/alerter'], function (alerter) {


    var viewModel = function() {
        var self = this;
        
        self.init = function() {
            alerter.ShowGreeting();
        };        
    };

    return new viewModel();

});