﻿define(['knockout', 'Data/userRepository'], function(ko, repo) {

    var viewModel = function () {
        var self = this;
        
        self.firstName = ko.observable('');
        self.lastName = ko.observable('');
        
        self.getData = function(userId) {
            var user = repo.Get(userId);
            self.firstName(user.firstName);
            self.lastName(user.lastName);
        };
        
        self.saveData = function() {
            repo.Save(self.firstName(), self.lastName());
        };

        self.init = function () {
            self.getData(123);
            ko.applyBindings(self);
        };
    };

    return new viewModel();

});

