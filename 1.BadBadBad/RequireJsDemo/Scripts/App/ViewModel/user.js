﻿//depends directly on jquery
//depends directly on knockout
//depends directly on userRepository.js
//depends indirectly on alerter.js

var ViewModel = function () {
    var self = this;
        
    self.firstName = ko.observable('');
    self.lastName = ko.observable('');
        
    self.getData = function(userId) {
        var user = getUser(userId);
        self.firstName(user.firstName);
        self.lastName(user.lastName);
    };
        
    self.saveData = function() {
        saveUser(self.firstName(), self.lastName());
    };
};
    
$(function () {
    var myViewModel = new ViewModel();
    myViewModel.getData(123);
    ko.applyBindings(myViewModel);

});

