﻿var ViewModel = function (repo) {
    var self = this;
        
    self.firstName = ko.observable('');
    self.lastName = ko.observable('');
        
    self.getData = function(userId) {
        var user = repo.Get(userId);
        self.firstName(user.firstName);
        self.lastName(user.lastName);
    };
        
    self.saveData = function() {
        repo.Save(self.firstName(), self.lastName());
    };
};
    
$(function () {
    var alerter = new Alerter();
    var repo = new Repository(alerter);
    var myViewModel = new ViewModel(repo);
    myViewModel.getData(123);
    ko.applyBindings(myViewModel);

});

