﻿require.config({
    paths: {
        Data: 'App/Data',
        Page: 'App/Page',
        ViewModel: 'App/ViewModel',
        Notification: 'App/Notification',
        jquery: 'Lib/jquery-1.9.1',
        knockout: 'Lib/knockout-2.2.1'
    },
    baseUrl: "/Scripts"
});
